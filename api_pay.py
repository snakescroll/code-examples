# -*- coding: utf-8 -*-

import hmac
import json
import base64
import hashlib

from copy import copy
from tornado import web, gen
from datetime import datetime
from SECRET01 import SECRET
from pymongo import ReturnDocument
from mind.your.own.business import Status0
from handlers.abstract import BaseHandler
from models.messaging_models import send_message

__all__ = ['ApiPayFailHandler', 'ApiPaySuccessHandler']


'''Фреймворк Tornado.
Генерация цифровой подписи для передачи криптографически зашифрованного
сообщения между сервисом и api платежной системы.
1. В классе CreateSignature происходит вычисление кода HMAC,
полученной строки signature с использованием функции хеширования SHA-512
и SECRET -- секретного ключа проекта по правилам указанным в технической документации
платежной системы.
2. ApiPaySuccessHandler и ApiPayFailHandler хендлеры обрабатывающие входящие post запросы
об успешной или не успешной операции и производящие либо не производящие те или иные
изменения в MongoDB. Наследуемые от класса ApiPayAbstract, в котором по умолчанию предусмотрена
проверка подлинности каждого входященго сообщения.
'''


class CreateSignature:

    def __init__(self, json_dict):
        self.json_dict = json_dict

    def get_values(self, d):
        for k, v in d.items():
            if isinstance(v, dict):
                for sub_k in self.get_values(v):
                    yield '%s:%s' % (k, sub_k)
            elif isinstance(v, list):
                for i, val in enumerate(v):
                    for sub_k in self.get_values(val):
                        yield '%s:%s:%s' % (k, i, sub_k)
            elif isinstance(v, bool):
                if bool(v):
                    yield '%s:%s' % (k, 1)
                else:
                    yield '%s:%s' % (k, 0)
            elif v is None:
                yield '%s:' % k
            else:
                yield '%s:%s' % (k, v)

    def create_signature(self):
        try:
            self.json_dict.pop('signature')
        except KeyError:
            pass
        try:
            self.json_dict.get('general', {}).pop('signature')
        except KeyError:
            pass

        dict_to_str = ';'.join(map(str, sorted(self.get_values(
            self.json_dict))))
        signature = base64.b64encode(hmac.new(
            bytes(SECRET).encode('utf-8'),
            msg=bytes(dict_to_str).encode('utf-8'),
            digestmod=hashlib.sha512).digest())

        return signature


class ApiPayAbstract(BaseHandler):

    def __init__(self, *args, **kwargs):
        self.json_dict = {}
        self.payment_id = unicode()
        super(BaseHandler, self).__init__(*args, **kwargs)

    def check_xsrf_cookie(self):
        pass

    def check_signature(self):
        input_signature = self.json_dict.get('signature')
        if not isinstance(input_signature, (str, unicode)):
            self.captureMessage('Signature can\'t be found')
            self.write_err(_status=400, _message='Signature can\'t be found')
        cs = CreateSignature(self.json_dict)
        true_signature = cs.create_signature()
        if input_signature == true_signature:
            return True
        return False

    @gen.coroutine
    def save_payments(self, status, order=None):
        amount = self.json_dict.get('pay', {}
                                    ).get('sum', {}
                                          ).get('amount', {})

        if not isinstance(amount, (int, float)):
            self.write_err(_status=400, _message='Wrong amount')

        if not order:
            order = yield self.application.asyncdb.orders.find_one(
                {'tx_id': self.payment_id})

        guest = order.get('guest')
        hotel = order.get('hotel')
        name = order.get('name')

        payment = {
            'status': status,
            'item_type': 'order',
            'amount': amount / 100.0,
            'guest': guest,
            'item_payment_id': '',
            'date': datetime.utcnow(),
            'hotel': hotel,
            'api_type': 'C',
            'name': name
        }

        yield self.application.asyncdb.payments.insert(payment)

    @gen.coroutine
    def post(self):
        self.json_dict = json.loads(self.request.body)
        copy_json_dict = copy(self.json_dict)
        if not self.check_signature():
            self.captureMessage('Wrong signature')
            self.write_err(_status=400, _message='Wrong signature')

        yield self.application.asyncdb.responses.insert(
            copy_json_dict)

        self.payment_id = self.json_dict.get('operation', {}
                                             ).get('provider', {}
                                                   ).get('payment_id', {})


class ApiPayFailHandler(ApiPayAbstract):

    @gen.coroutine
    def post(self):
        yield super(ApiPayFailHandler, self).post()
        yield self.save_payments(Status0.PAY_FAILED)
        self.write_json({'message': 'Success'})


class ApiPaySuccessHandler(ApiPayAbstract):

    @gen.coroutine
    def post(self):
        yield super(ApiPaySuccessHandler, self).post()

        order = yield self.application.asyncdb.orders.find_one_and_update(
            {'tx_id': self.payment_id},
            {
                '$set': {'status': Status0.PAID},
                '$push': {
                    'status_changes': {
                        'status': Status0.PAID,
                        'message': None,
                        'date': datetime.utcnow(),
                        'comment': 'Paid by apipay',
                        'initiator': 'apipay'
                    }
                }
             }, upsert=False, return_document=ReturnDocument.AFTER)

        if not order:
            self.captureMessage('Wrong ID')
            self.write_err(_status=400, _message='Wrong ID')

        send_message(self.application, order)
        yield self.save_payments(Status0.PAID, order)
        self.write_json({'message': 'Success'})
