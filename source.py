# -*- coding: utf-8 -*-

from __future__ import annotations
from copy import copy
from dataclasses import dataclass
from typing import Dict, Optional, Tuple


__all__ = ['Status0']


''' dev версия модуля назначения статусов
файла source.py действующего микросервиса.'''


@dataclass
class Status0:
    __code: Optional[str] = None
    __title: Optional[Dict[str, str]] = None
    __description: Optional[Dict[str, str]] = None
    __next_status: Optional[Tuple[Status0, ...]] = None

    @property
    def code(self):
        return self.__code

    @code.setter
    def code(self, val) -> None:
        if self.__code is None:
            self.__code = val
        else:
            raise AttributeError(
                'Attribute %s can\'t be changed' % self.__code)

    @property
    def title(self):
        return self.__title.copy()

    @title.setter
    def title(self, val) -> None:
        if self.__title is None:
            self.__title = val
        else:
            raise AttributeError(
                'Attribute %s can\'t be changed' % self.__code)

    @property
    def description(self):
        return self.__description.copy()

    @description.setter
    def description(self, val) -> None:
        if self.__description is None:
            self.__description = val
        else:
            raise AttributeError(
                'Attribute %s can\'t be changed' % self.__code)

    @property
    def next_status(self):
        return self.__next_status

    @next_status.setter
    def next_status(self, val) -> None:
        if self.__next_status is None:
            self.__next_status = val
        else:
            raise AttributeError(
                'Attribute %s can\'t be changed' % self.__code)

    def __set_next_status(self, *args: Status0) -> None:
        if not self.__code:
            raise AttributeError('code can\'t be None')
        if not self.__title:
            raise AttributeError('title can\'t be None')
        if not self.__description:
            raise AttributeError('description can\'t be None')

        if hasattr(self, self.__code):
            raise AttributeError(
                'Attribute %s can\'t be changed' % self.__code)
        self.__next_status = args
        super().__setattr__(self.__code, True)

    @staticmethod
    def set_status_data() -> Dict[str, Status0]:
        new = Status0(
            'NEW',
            {'en': 'new', 'ru': u'новый'},
            {'en': 'new_dsc'})

        cancelled = Status0(
            'CANCELLED',
            {'en': 'cancelled', 'ru': u'отмененный'},
            {'en': 'cancelled_dsc'})

        pay_pending = Status0(
            'PAY_PENDING',
            {'en': 'pay_pending'},
            {'en': 'pay_pending'})

        pay_failed = Status0(
            'PAY_FAILED',
            {'en': 'pay_failed'},
            {'en': 'pay_failed'})

        paid = Status0(
            'PAID',
            {'en': 'paid'},
            {'en': 'paid'})

        accepted = Status0(
            'ACCEPTED',
            {'en': 'accepted'},
            {'en': 'accepted'})

        rejected = Status0(
            'REJECTED',
            {'en': 'rejected'},
            {'en': 'rejected'})

        completed = Status0(
            'COMPLETED',
            {'en': 'completed'},
            {'en': 'completed'})

        failed = Status0(
            'FAILED',
            {'en': 'failed'},
            {'en': 'failed'})

        new.__set_next_status(cancelled, pay_pending, pay_failed)
        cancelled.__set_next_status()
        pay_pending.__set_next_status()
        pay_failed.__set_next_status()
        paid.__set_next_status(completed, failed)
        accepted.__set_next_status()
        rejected.__set_next_status()
        completed.__set_next_status()
        failed.__set_next_status()

        status_data = dict(
            NEW=new,
            CANCELLED=cancelled,
            PAY_PENDING=pay_pending,
            PAY_FAILED=pay_failed,
            PAID=paid,
            ACCEPTED=accepted,
            REJECTED=rejected,
            COMPLETED=completed,
            FAILED=failed)
        return status_data


STATUSES = Status0().set_status_data()

if __name__ == '__main__':
    print(STATUSES)
